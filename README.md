# zurb/[foundation-cli](https://www.npmjs.com/package/foundation-cli)

The command line installer for Foundation Sites, Emails, Apps

## Official documentation
* [*Two Ways to Get Started*](https://foundation.zurb.com/emails/getting-started.html)
* [*Getting Started with CSS: Get started with the CSS version of Foundation for Emails*](https://foundation.zurb.com/emails/docs/css-guide.html)
* [*Bring Your Email Styles Inline: Use the Foundation for Emails Inliner to do it in one fell swoop*](https://foundation.zurb.com/emails/inliner-v2.html)
* [*Getting Started with Sass: Get started with the Sass-powered ZURB Stack for writing HTML emails*](https://foundation.zurb.com/emails/docs/sass-guide.html)
* [*Installation*](https://foundation.zurb.com/sites/docs/installation.html#command-line-tool.html)
* [zurb/foundation-cli](https://github.com/zurb/foundation-cli)

## Unofficial documentatation
* [zurb webpack](https://www.google.com/search?q=zurb+webpack)
* [*Use Webpack to include Zurb Foundation via NPM*](https://stackoverflow.com/questions/34107697/use-webpack-to-include-zurb-foundation-via-npm)
* [*How-To: Foundation 6 with Webpack using Sass*](https://networksynapse.net/how-to-foundation-6-with-webpack-using-sass/)

## Bugs
### "foundation-cli" cannot install "emails" 2019-07
'foundation new --directory emails --framework emails' produce this message
'npm ERR! git rev-list -n1 4.0: fatal: ambiguous argument '4.0': unknown revision or path not in the working tree.
npm ERR! git rev-list -n1 4.0: Use '--' to separate paths from revisions, like this:
npm ERR! git rev-list -n1 4.0: 'git <command> [<revision>...] -- [<file>...]'
npm ERR! git rev-list -n1 4.0: 

There were some problems during the installation.

 ✓ New project folder created.
 ✗ Node modules not installed. Try running npm install manually.
 ✓ Bower components installed.
'

#### 2019-11: foundation-cli may be outdated or abandonned.
* Zurb Foundation version for Sites and Emails are different.
* [sites](https://foundation.zurb.com/sites)
* [emails](https://foundation.zurb.com/emails)
* Probably better use the following packages
* [foundation-sites](https://www.npmjs.com/package/foundation-sites)
* [foundation-emails](https://www.npmjs.com/package/foundation-emails)
* [*Install with Package Managers*
  ](https://foundation.zurb.com/sites/docs/installation.html)